# BlockChain Contest 2021

## Project description

This project was carried out as part of the 2021 Campus Contest and the goal was to create a beginning of block-chain in Python.  

During this contest, object-oriented programming skills was use.  

**Definition :** Blockchain is a system for storing records of transactions using digital currencies, that can be accessed by linked computers

Explanation :
    
![alt text](https://static.latribune.fr/full_width/1020548/blockchain-mode-d-emploi.jpg
)

Functionalities :  
- Create wallet
- Create block
- Create chain
- Realize transactions between two wallets on the chain and in a specific block
- Check information like block hash or uuid wallet is correct

## Architecture

- main.py : *project entry point, used to test the program*
    - content : *database of the project*
        - blocs : *folder that contains all JSON files for each block created*
        - wallets : *folder that contains all JSON files for each wallet created*
    - classes : *all classes*
        - block.py : *block class*
        - chain.py : *chain class*
        - wallet.py : *wallet class*
    - settings :
        - settings.py : *file that contain the maximum token constant for chain*

In the main.py, you can execute every code to test all classes and the blockchain. 
You can also execute already existing code that test for you all classes and the blockchain, you can see comments to explain to you the results.

## Project Operation

1) You can create wallet for each user
    - Each wallet has a unique uuid, a balance, a history of all his transactions
    
2) You can create block in a chain
    - Each block has a unique hash (SHA256 encode), a parent hash, a base-hash (use to generate the block hash), a list of all block's transactions
    
3) You can create a chain
    - Each chain contain blocks and all transactions goes to the chain to a block.
    - When a block achieved 250000 octets, a new block is created.
    
4) You can create transactions between two wallets like token transfer (transmitter and receiver balance will be updated)

**Storage :**  
All data of the blockchain is store in the project folder "content".
- Each created wallet is saved in a JSON file content in this path "content/wallets".
- Each created block is saved in a JSON file content in this path "content/blocs".

**Classes :**
- Wallet :
    - constructor => wallet = Wallet()
    - attributes :
        - unique_id (id for each wallet)
        - balance (amount of wallet's balance)
        - history (list of all transactions for the wallet)
    
- Block :
    - constructor => block = Block(
                                base_hash: int,
                                hash: str,
                                parent_hash: str,
                                transactions: []
      )
      
    - attributes :
        - base_hash (number of iteration to find the hash)
        - hash (unique hash for a block)
        - parent_hash (block parent hash)
        - transaction (list of all block transactions)
    
- Chain :
    - constructor => chain = Chain()
    - attributes :
        - blocks (list of chain's blocks)
        - last_transaction_number (last transaction id)
        - last_block_nonce (last iteration number to find the last block hash)
        - last_block_hash (last hash block created)
        - parent_block_hash (last parent block hash)
      

      
    
## Project Installation
Prerequisite : 
- Python V3.9.4 or more 

**First step :** clone the project from my gitlab on your computer
(*see the documentation : https://git-scm.com/docs/git-clone*)  

**Second step :**
Respect the project architecture, if the folder "content" is not created in the project root, please create it. The folder "wallets" and "blocs" are automatically generated.  

**Third step :** 
Open the project in an IDE like PyCharm for example to see all comments, add your own test and execute the code.  

**Or**  

Open a terminal and launch the main.py file at the project root like this :
```python
python main.py
```  

Now, you can play with this blockChain and add your own code.


## Tips

In the folder "content/blocs", you have already a created block whose the size is more than 250000 octets. You can test to do transactions on this block and you will see the maximum reach message and a new block creation.  

**Enjoy !**