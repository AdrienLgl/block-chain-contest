import uuid
import json
import os
import datetime


class Wallet:

    def __init__(self):
        self.unique_id = ""
        self.balance = 100
        self.history = []

    def generate_unique_id(self):
        wallet_uuid = str(uuid.uuid4())
        # Check if id already exist
        while self.uuid_exist(wallet_uuid):
            wallet_uuid = str(uuid.uuid4())
        # If not exist, create uuid
        self.unique_id = wallet_uuid

    def add_balance(self, value: int):
        self.balance += value
        self.send(1, value)
        self.save()

    def sub_balance(self, value: int):
        self.balance -= value
        self.send(0, value)
        self.save()

    def send(self, transaction_type: int, amount: int):
        # Add to history
        if transaction_type == 0:
            transaction = "withdrawal"
        elif transaction_type == 1:
            transaction = "addition"
        else:
            transaction = "other"
        self.history.append(
            {
                "transaction_type": transaction,
                "amount": amount,
                "date": str(datetime.datetime.now())
            }
        )

    def save(self):
        if not os.path.exists("content/wallets"):
            os.mkdir("content/wallets")
        json_data = json.dumps(self.__dict__)
        json_file = open("content/wallets/" + self.unique_id + ".json", "wt")
        json_file.write(json_data)
        json_file.close()

    def load(self, wallet_uuid):
        if not os.path.exists("content/wallets"):
            os.mkdir("content/wallets")
        if self.uuid_exist(wallet_uuid):
            json_file = open("content/wallets/" + wallet_uuid + ".json", "r")
            wallet_data = json.load(json_file)
            self.unique_id = wallet_data["unique_id"]
            self.balance = wallet_data["balance"]
            self.history = wallet_data["history"]
            json_file.close()
        else:
            print("Wallet UUID unknown, please retry with other UUID")
            return

    @staticmethod
    def uuid_exist(wallet_uuid) -> bool:
        return os.path.isfile("content/wallets/" + wallet_uuid + ".json")
