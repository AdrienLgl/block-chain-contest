import hashlib
import os
from classes.block import Block
from classes.wallet import Wallet
import settings.settings
from typing import Union


class Chain:

    def __init__(self):
        self.blocks = []
        self.last_transaction_number = 0
        self.last_block_nonce = 0
        # Last iteration to find a hash for the last block
        self.last_block_hash = "00"
        # Last block hash
        self.parent_block_hash = "00"
        # Parent block hash

    def generate_hash(self):
        nonce = 0
        block_hash = self.create_hash(nonce)
        print("Block is creating...")
        while not self.verify_hash(block_hash):
            nonce += 1
            block_hash = self.create_hash(nonce)
        print("\033[92mNew block created ! ( " + block_hash + " ) \033[0m")
        self.parent_block_hash = str(self.last_block_hash)
        self.last_block_nonce = nonce
        self.last_block_hash = block_hash

    @staticmethod
    def create_hash(nonce):
        sha = hashlib.sha256(
            f'{nonce}'.encode()
        ).hexdigest()
        return sha

    @staticmethod
    def verify_hash(block_hash) -> bool:
        hash_exist = os.path.isfile("content/blocs/" + block_hash + ".json")
        hash_is_valid = block_hash[:4] == "0000"
        if hash_exist is False and hash_is_valid is True:
            return True
        else:
            return False

    def add_block(self):
        self.generate_hash()
        new_block = Block(
            self.last_block_nonce,
            self.last_block_hash,
            self.parent_block_hash,
            []
        )
        self.blocks.append(new_block)
        new_block.save()

    def get_block(self, block_hash) -> Union[Block, None]:
        if self.block_exist(block_hash):
            for block in self.blocks:
                if block.hash == block_hash:
                    return block
        else:
            print("\033[91mBlock hash unknown, "
                  "please retry with other hash \033[0m")
            return None

    def add_transaction(
            self,
            transmitter_wallet: Wallet,
            receiver_wallet: Wallet,
            price,
            block: Block
    ):
        # Check if the block is available
        if block.get_weight() <= 256000 and block.check_hash():
            if self.verify_transaction(
                    transmitter_wallet,
                    receiver_wallet,
                    price
            ):
                self.last_transaction_number += 1
                block.add_transaction(
                    self.last_transaction_number,
                    transmitter_wallet,
                    receiver_wallet,
                    price
                )
                # Save in the chain
                self.save_block_on_chain(block)
                print("\033[92mTransaction close with success \033[0m")
            else:
                print("\033[91m"
                      "Impossible to complete the transaction, "
                      "check the transmitter or receiver balance "
                      "\033[0m"
                      )
        else:
            # Create a new block if this block is too heavy
            print("\033[91mMaximum size reached for this block, "
                  "new block is creating... \033[0m")
            self.add_block()
            self.add_transaction(
                                 transmitter_wallet,
                                 receiver_wallet,
                                 price,
                                 self.blocks.pop()
                                 )

    @staticmethod
    def block_exist(block_hash) -> bool:
        return os.path.isfile("content/blocs/" + block_hash + ".json")

    @staticmethod
    def verify_transaction(
            transmitter_wallet: Wallet,
            receiver_wallet: Wallet,
            amount_transaction
    ):
        wallet = Wallet()
        # Check if the transmitter and receiver exist
        if wallet.uuid_exist(transmitter_wallet.unique_id) is True and \
                wallet.uuid_exist(receiver_wallet.unique_id) is True:
            # Check the transmitter and receiver balance
            if transmitter_wallet.balance >= amount_transaction and \
                    receiver_wallet.balance + \
                    amount_transaction <= settings.settings.MAX_TOKEN_NUMBER:
                return True
        return False

    def find_transaction(self, transaction_id) -> any:
        for block in self.blocks:
            if block.get_transaction(transaction_id) is not None:
                return block
        print("\033[91mTransaction id unknown, "
              "please retry with other id \033[0m")
        return None

    def get_last_transaction_number(self) -> int:
        return self.last_transaction_number

    def save_block_on_chain(self, block_to_save: Block):
        for index, block in enumerate(self.blocks):
            if block.hash == block_to_save.hash:
                self.blocks[index] = block_to_save
        self.blocks.append(block_to_save)
