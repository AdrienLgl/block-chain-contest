import hashlib
import os
import json
import datetime
from classes.wallet import Wallet


class Block:
    def __init__(
            self,
            base_hash: int,
            hash: str,
            parent_hash: str,
            transactions: []
    ):
        self.base_hash = base_hash
        self.hash = hash
        self.parent_hash = parent_hash
        self.transactions = transactions

    def check_hash(self) -> bool:
        return self.create_hash(self.base_hash) == self.hash

    def add_transaction(
            self,
            transaction_id,
            transmitter_wallet: Wallet,
            receiver_wallet: Wallet,
            price
    ):
        transaction = {
            "transaction_id": transaction_id,
            "transmitter": transmitter_wallet.unique_id,
            "receiver": receiver_wallet.unique_id,
            "amount": price,
            "block": self.hash,
            "date": str(datetime.datetime.now())
        }
        # Change transmitter and receiver wallet balances
        transmitter_wallet.sub_balance(price)
        receiver_wallet.add_balance(price)
        # Add transaction to all transactions block
        self.transactions.append(transaction)
        self.save()

    def get_transaction(self, transaction_number):
        for item in self.transactions:
            if item["transaction_id"] == transaction_number:
                return item
        return None

    def get_weight(self) -> float:
        return os.path.getsize("content/blocs/" + self.hash + ".json")

    def save(self):
        if not os.path.exists("content/blocs"):
            os.mkdir("content/blocs")
        json_data = json.dumps(self.__dict__)
        json_file = open("content/blocs/" + self.hash + ".json", "wt")
        json_file.write(json_data)
        json_file.close()

    def load(self, block_hash):
        if not os.path.exists("content/blocs"):
            os.mkdir("content/blocs")
        if self.block_exist(block_hash):
            json_file = open("content/blocs/" + block_hash + ".json", "r")
            block_data = json.load(json_file)
            self.hash = block_data["hash"]
            self.base_hash = block_data["base_hash"]
            self.parent_hash = block_data["parent_hash"]
            self.transactions = block_data["transactions"]
        else:
            print("Block hash unknown, please retry with other hash")
            return

    @staticmethod
    def block_exist(block_hash) -> bool:
        return os.path.isfile("content/blocs/" + block_hash + ".json")

    @staticmethod
    def create_hash(base) -> str:
        block_hash = hashlib.sha256(str(base).encode()).hexdigest()
        return str(block_hash)
