from classes.chain import Chain
from classes.wallet import Wallet
from classes.block import Block


def main():
    print("""\033[93m
    ########################################
    # Wallets creation (wallet class test) #
    ########################################
    \033[0m""")
    wallet_01 = Wallet()
    wallet_01.generate_unique_id()
    wallet_01.save()
    wallet_02 = Wallet()
    wallet_02.generate_unique_id()
    wallet_02.save()
    wallet_03 = Wallet()
    wallet_03.generate_unique_id()
    wallet_03.save()
    # 3 UUID not equals
    print(wallet_01.unique_id)
    print(wallet_02.unique_id)
    print(wallet_03.unique_id)
    # Balance to 100 at the creation
    print(wallet_01.balance)
    wallet_01.add_balance(1500)
    # Equal to 1600
    print(wallet_01.balance)
    wallet_01.sub_balance(500)
    # Equal to 1100
    print(wallet_01.balance)
    # History empty
    print(wallet_02.history)
    # History with actions
    print(wallet_01.history)
    # Load a wallet
    new_wallet = Wallet()
    new_wallet.load(wallet_01.unique_id)
    print("Should be the same uuid between "
          "the wallet 1 and the new wallet : " +
          wallet_01.unique_id + " = " + new_wallet.unique_id)

    print("""\033[93m
        ######################################
        # Block creation (block class test)  #
        ######################################
        \033[0m""")

    test_block = Chain()
    test_block.generate_hash()
    first_block = Block(
        test_block.last_block_nonce,
        test_block.last_block_hash,
        test_block.parent_block_hash,
        []
    )

    # Check if the block hash is correct
    print("The block should be correct : " + str(first_block.check_hash()))
    first_block.base_hash = "0"
    print("The block should be not correct : " + str(first_block.check_hash()))

    # Add transaction to a block
    first_block.add_transaction(
        1,
        wallet_01,
        wallet_02,
        500
    )
    print(first_block.transactions)

    # Get a transaction with id
    # Should return the transaction
    print(first_block.get_transaction(1))
    # Should return None
    print(first_block.get_transaction(2))

    # Get block weight
    print("Size of this block (" + first_block.hash + ") is : " +
          str(first_block.get_weight()) + " octets")

    print("""\033[93m
    ##############################################
    # Block Creation on chain (chain class test) #
    ##############################################
    \033[0m""")
    chain = Chain()
    chain.add_block()
    # Try to find a unknown block
    if chain.get_block("12345") is not None:
        block: Block = chain.get_block("12345")
    else:
        # Try to find the last created block
        block = chain.get_block(chain.last_block_hash)
    print("Load the last block created : " + block.hash)
    print("""\033[93m
    #################################
    # Transaction between 2 wallets #
    #################################
    \033[0m""")
    print("Emitter balance : " + str(wallet_01.balance))
    print("Receiver balance : " + str(wallet_02.balance))
    # Add transaction to the chain and the first block
    chain.add_transaction(wallet_01, wallet_02, 150, block)
    print("The transaction's sum is 150")
    # Check if the transaction is a success
    print("Emitter balance : " + str(wallet_01.balance))
    print("Receiver balance : " + str(wallet_02.balance))
    # Check the history
    print("Emitter history : " + str(wallet_01.history))
    print("Receiver history : " + str(wallet_02.history))
    print("Transaction between " + wallet_01.unique_id +
          " and " + wallet_02.unique_id)
    print(block.transactions)

    # Impossible transaction because the emitter wallet
    # does not have enough tokens
    chain.add_transaction(wallet_01, wallet_02, 10000, block)

    # Transaction with a block too heavy
    chain.add_block()
    block_too_heavy = chain.get_block(chain.last_block_hash)
    # Load a block too heavy
    # Please, put the id of a heavy block (more than 250000 octets)
    hash_to_heavy = "put your variable here"
    block_too_heavy.load(hash_to_heavy)
    # Should create a new block
    chain.add_transaction(wallet_01, wallet_02, 50, block_too_heavy)

    # Find last transaction
    last_block = chain.get_block(chain.last_block_hash)
    print(last_block.transactions)
    # Get last transaction id
    last_transaction_id = chain.get_last_transaction_number()
    last_transaction_block = chain.find_transaction(last_transaction_id)
    print("The block hash should be equal to the last hash created : "
          "Last hash = " + chain.last_block_hash +
          " | Last transaction block hash = " + last_transaction_block.hash)


if __name__ == "__main__":
    main()
